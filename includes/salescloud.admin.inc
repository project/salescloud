<?php

/**
 * @file
 * Code required only when rendering the SalesCloud admin form.
 */

/**
 * SalesCloud Admin Form; Adjust SalesCloud auth settings.
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 *
 * @ingroup forms
 *
 * @see salescloud_admin_form_submit()
 */
function salescloud_admin_form($form, array &$form_state) {
  $form = array();

  $settings = salescloud_settings();

  $form['client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => $settings['salescloud_client_id'],
    '#max_length' => 32,
    '#required' => TRUE,
  );

  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => $settings['salescloud_username'],
    '#max_length' => 32,
    '#required' => TRUE,
  );

  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => $settings['salescloud_password'],
    '#max_length' => 32,
    '#required' => TRUE,
  );

  $form['client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret'),
    '#default_value' => $settings['salescloud_client_secret'],
    '#max_length' => 32,
  );

  $form['scope'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Scope'),
    '#default_value' => $settings['salescloud_scope'],
    '#options' => salescloud_scope_options(),
    '#required' => TRUE,
  );

  if (!empty($settings['salescloud_username']) &&
    !empty($settings['salescloud_password']) &&
    !empty($settings['salescloud_client_id']) &&
    !empty($settings['salescloud_scope'])) {

    $connect = salescloud_connect();
    if (isset($connect['data']['user']['uid'])
      && $connect['data']['user']['uid'] > 0) {
      drupal_set_message(t('Successfully connected to SalesCloud!'));
    }
    else {
      drupal_set_message(t('Failed connecting to SalesCloud, please verify your
      Authentication data!'), 'error');
    }

  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Submit handler for adjust SalesCloud auth settings.
 */
function salescloud_admin_form_submit($form, $form_state) {

  $settings = array(
    'salescloud_client_id' => $form_state['values']['client_id'],
    'salescloud_client_secret' => $form_state['values']['client_secret'],
    'salescloud_username' => $form_state['values']['username'],
    'salescloud_password' => $form_state['values']['password'],
  );

  $scopes = array();
  foreach ($form_state['values']['scope'] as $key => $value) {
    if ($value) {
      $scopes[$key] = $value;
    }
  }
  $settings['salescloud_scope'] = $scopes;

  variable_set('salescloud_settings', $settings);

}

<?php

/**
 * @file
 * Exposes global functionality for SalesCloud.
 */

/**
 * Implements hook_permission().
 */
function salescloud_permission() {
  $return = array();

  $return['administer salescloud'] = array(
    'title' => t('Administer SalesCloud'),
    'description' => t('Adjust sensitive settings such as authentication
     information to connect to SalesCloud'),
    'restrict access' => TRUE,
  );

  return $return;
}

/**
 * Implements hook_menu().
 */
function salescloud_menu() {
  $items = array();

  $items['admin/config/services/salescloud'] = array(
    'title' => 'SalesCloud',
    'description' => 'Connector for SalesCloud services.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('salescloud_admin_form'),
    'access arguments' => array('administer salescloud'),
    'file' => 'includes/salescloud.admin.inc',
  );

  $items['admin/config/services/salescloud/settings'] = array(
    'title' => 'Settings',
    'description' => 'Connector for SalesCloud services.',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  return $items;
}

/**
 * Instantiates a SalesCloud Singleton ready for use and returns it.
 */
function salescloud() {
  $settings = salescloud_settings();
  return SalesCloud::instance(
    $settings['salescloud_client_id'],
    $settings['salescloud_client_secret'],
    $settings['salescloud_scope'],
    $settings['salescloud_username'],
    $settings['salescloud_password']
  );
}

/**
 * Creates SalesCloud Entity.
 *
 * @param string $entity
 *   The entity to create.
 * @param array $data
 *   The data of the entity.
 *
 * @return array
 *   Response of the Create operation.
 */
function salescloud_create($entity, array $data) {
  return salescloud()->create($entity, $data);
}

/**
 * Gets SalesCloud Entity.
 *
 * @param string $entity
 *   The entity to get.
 * @param int $id
 *   The entity id.
 *
 * @return array
 *   Response of the Get operation.
 */
function salescloud_get($entity, $id) {
  return salescloud()->get($entity, $id);
}

/**
 * Updates SalesCloud Entity.
 *
 * @param string $entity
 *   The entity to update.
 * @param string $id
 *   The entity id to update.
 * @param array $data
 *   The data to update.
 *
 * @return array
 *   Response of the Update operation.
 */
function salescloud_update($entity, $id, array $data) {
  return salescloud()->update($entity, $id, $data);
}

/**
 * Deletes SalesCloud Entity.
 *
 * @param string $entity
 *   The entity to delete.
 * @param int $id
 *   The entity id to delete.
 *
 * @return array
 *   Response of the Delete operation.
 */
function salescloud_delete($entity, $id) {
  return salescloud()->delete($entity, $id);
}

/**
 * Indexes SalesCloud Entities.
 *
 * @param string $entity
 *   The entity to index.
 * @param array $options
 *   The query options of the index operation.
 *
 * @return array
 *   Response of the Index operation.
 */
function salescloud_index($entity, array $options = NULL) {
  return salescloud()->index($entity, $options);
}

/**
 * Function fetches current connection information with SalesCloud.
 */
function salescloud_connect() {
  return salescloud()->connect();
}

/**
 * Wrapper function to get SalesCloud Settings.
 */
function salescloud_settings() {
  return variable_get('salescloud_settings',
      array(
        'salescloud_client_id' => '',
        'salescloud_client_secret' => '',
        'salescloud_company' => '',
        'salescloud_scope' => array(),
        'salescloud_username' => '',
        'salescloud_password' => '',
      )
  );
}

/**
 * Simple function that returns an associative array of scopes.
 */
function salescloud_scope_options() {
  return array(
    'payment_create' => t('Create payment transactions.'),
    'product_index' => t('Index products.'),
    'product_create' => t('Create products.'),
    'product_update' => t('Update products.'),
    'product_delete' => t('Delete products.'),
  );
}

-- SUMMARY --

SalesCloud uses the Commerce Guys Oauth2 and Guzzle 5 to connect your drupal
site to a SalesCloud API.

PHP Libraries are already included under the vendor directory.

-- Requirements --
Composer Manager Module
Composer (Server Side)
PHP version at least 5.4
Curl version at least 7.16.2
allow_url_fopen enabled by php

-- INSTALLATION --

  1. Download and enable the module.
  2. Satisfy requirements and dependencies if necessary
  3. Configure at Administer > Configuration > Services > SalesCloud
     (requires administer salescloud permission)

-- INSTALLATION VIA DRUSH --

  Simply enable via drush en salescloud.

  Requirements are defined via hook_requirements and so drush should detect if
  any requirements are not met and display the relevant alerts.

-- SECURITY CONCERN --

If you find or suspect your drupal is compromised please contact
support@proton.is and alert them to your situation. We will disable your
API Access and reset your Client ID and Password.

-- TROUBLE SHOOTING --

Please visit the status report page and check for any unmet requirements.
This module is only required by other modules, there is no need to install this
module if not required by another module.

This module requires the Composer Manager module. Please make sure your
drupal installation has the required permissions to allow Composer Manager to
create directories in sites/all/vendor, sites/all/vendor/* and your public file
system.

If you run into issues with Composer Manager during install you may need to
delete the created directories after uninstall so that Composer Manager may
recreate them correctly.

When installing SalesCloud module by drush you may run into an issue with
composer versioning as drush uses its own composer version. You may read more
and fix this issue here: https://www.drupal.org/node/2490774

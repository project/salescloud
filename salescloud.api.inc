<?php

/**
 * @file
 * Code required to communicate with SalesCloud Rest API.
 */

define("SALESCLOUD_BASE_URL", 'https://api.salescloud.is');

use GuzzleHttp\Client;
use CommerceGuys\Guzzle\Oauth2\GrantType\GrantTypeInterface;
use CommerceGuys\Guzzle\Oauth2\GrantType\RefreshToken;
use CommerceGuys\Guzzle\Oauth2\Oauth2Subscriber;
use GuzzleHttp\Exception\RequestException;
use CommerceGuys\Guzzle\Oauth2\AccessToken;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Collection;

/**
 * Class SalesCloud.
 */
class SalesCloud {

  static $instance;
  /**
   * Method to get singleton.
   *
   * @param string $client_id
   *   Client ID provided by SalesCloud.
   * @param string $secret
   *   Client Secret provided by SalesCloud.
   * @param array $scope
   *   API Scopes to be used with the connection to SalesCloud.
   * @param string $username
   *   Username provided by SalesCloud.
   * @param string $password
   *   Password provided by SalesCloud.
   *
   * @return SalesCloud
   *   Returns a SalesCloud instance as a static singleton.
   */
  public static function instance($client_id, $secret, array $scope, $username, $password) {
    if (static::$instance === NULL) {
      static::$instance = new SalesCloud($client_id, $secret, $scope, $username, $password);
    }
    return static::$instance;
  }

  /**
   * SalesCloud constructor.
   *
   * @param string $client_id
   *   Client ID provided by SalesCloud.
   * @param string $secret
   *   Client Secret provided by SalesCloud.
   * @param array $scope
   *   API Scopes to be used with the connection to SalesCloud.
   * @param string $username
   *   Username provided by SalesCloud.
   * @param string $password
   *   Password provided by SalesCloud.
   */
  private function __construct($client_id, $secret, array $scope, $username, $password) {
    $this->base = SALESCLOUD_BASE_URL;
    $this->client_id = $client_id;
    $this->secret = $secret;
    $this->scope = $scope;
    $this->username = $username;
    $this->password = $password;
  }

  /**
   * Creates a Client.
   *
   * @return \GuzzleHttp\Client
   *   Returns a Client ready for use.
   */
  private function client() {
    $oauth2_client = new Client([
      'base_url' => $this->base,
      'defaults' => array(
        'headers' => array('Content-Type' => 'application/json'),
      ),
    ]);

    $config = array(
      'username' => $this->username,
      'password' => $this->password,
      'client_id' => $this->client_id,
      'scope' => implode(',', $this->scope),
      'json' => array(
        'body' => array(
          'username' => $this->username,
          'password' => $this->password,
          'client_id' => $this->client_id,
          'scope' => implode(',', $this->scope),
        ),
      ),
    );

    $token = new SalesCloudPasswordCredentials($oauth2_client, $config);
    $refresh_token = new RefreshToken($oauth2_client, $config);

    $oauth2 = new Oauth2Subscriber($token, $refresh_token);

    $client = new Client([
      'defaults' => array(
        'auth' => 'oauth2',
        'subscribers' => array($oauth2),
        'connect_timeout' => 2000,
        'allow_redirects' => TRUE,
        'headers' => array(
          'Content-Type' => 'application/json',
        ),
      ),
    ]);

    return $client;
  }

  /**
   * Performs an Index operation on the SalesCloud API.
   *
   * @param string $entity
   *   The entity to index.
   * @param null|array $options
   *   The options to use with the operation.
   *
   * @return array
   *   The response of the operation.
   */
  public function index($entity, $options = array()) {
    $client = $this::client();
    $query = $this->httpQuery($options);
    $return = array();

    try {
      $res = $client->get($this->base . '/api/2.0/entity_' . $entity . $query);
      $return = $this->getReturn($res, $res->json());
    }
    catch (RequestException $e) {
      if ($e->getResponse()) {
        $return = $this->getReturn($e->getResponse());
      }
    }

    return $return;
  }

  /**
   * Builds a http query.
   *
   * @param null|array $options
   *   Query options.
   *
   * @return string
   *   Formatted query for url.
   */
  private function httpQuery($options = array()) {
    return $options !== NULL ? '?' . http_build_query($options, '?') : '';
  }

  /**
   * Performs a Get operation on the SalesCloud API.
   *
   * @param string $entity
   *   The entity to get.
   * @param int $id
   *   The entity id to get.
   *
   * @return array
   *   The response of the operation.
   */
  public function get($entity, $id) {
    $return = array();

    $client = $this::client();

    try {
      $res = $client->get($this->base . '/api/2.0/entity_' . $entity . '/' . $id);
      $return = $this->getReturn($res, $res->json());
    }
    catch (RequestException $e) {
      if ($e->getResponse()) {
        $return = $this->getReturn($e->getResponse());
      }
    }

    return $return;
  }

  /**
   * Performs a Create operation on the SalesCloud operation.
   *
   * @param string $entity
   *   The entity to create.
   * @param array $data
   *   The data of the entity.
   *
   * @return array
   *   The response of the operation.
   */
  public function create($entity, array $data) {
    $return = array();

    $client = $this::client();

    try {
      $res = $client->post(
        $this->base . '/api/2.0/entity_' . $entity . '/',
        array('json' => $data)
      );

      $return = $this->getReturn($res, $res->json());
    }
    catch (RequestException $e) {
      if ($e->getResponse()) {
        $return = $this->getReturn($e->getResponse());
      }
    }

    return $return;
  }

  /**
   * Performs an Update operation on the SalesCloud API.
   *
   * @param string $entity
   *   The entity to update.
   * @param int $id
   *   The id of the entity.
   * @param array $data
   *   The array of the data to update.
   *
   * @return array
   *   The response of the update operation.
   */
  public function update($entity, $id, array $data) {
    $return = array();

    $client = $this::client();

    try {
      $res = $client->put(
        $this->base . '/api/2.0/entity_' . $entity . '/' . $id,
        array('json' => $data)
      );

      $return = $this->getReturn($res, $res->json());
    }
    catch (RequestException $e) {
      if ($e->getResponse()) {
        $return = $this->getReturn($e->getResponse());
      }
    }

    return $return;
  }

  /**
   * Performs a delete operation on the SalesCloud API.
   *
   * @param string $entity
   *   The entity to delete.
   * @param int $id
   *   The entity id to delete.
   *
   * @return array
   *   The response of the operation.
   */
  public function delete($entity, $id) {
    $return = array();

    $client = $this::client();

    try {
      $res = $client->delete($this->base . '/api/2.0/entity_' . $entity . '/' . $id);
      $return = $this->getReturn($res, $res->json());
    }
    catch (RequestException $e) {
      if ($e->getResponse()) {
        $return = $this->getReturn($e->getResponse());
      }
    }

    return $return;
  }

  /**
   * Performs a System Connect operation on the SalesCloud API.
   *
   * @return array
   *   Information about the current connection.
   */
  public function connect() {
    $return = array();

    $client = $this::client();

    try {
      $res = $client->post($this->base . '/api/2.0/system/connect', array('json' => array()));
      $return = $this->getReturn($res, $res->json());
    }
    catch (RequestException $e) {
      if ($e->getResponse()) {
        $return = $this->getReturn($e->getResponse());
      }
    }

    return $return;
  }

  /**
   * Builds and returns a standardized return array for Guzzle requests.
   *
   * @param object $response
   *   The guzzle response object.
   * @param array $data
   *   The data of the response as array if any.
   *
   * @return array
   *   Standardized response array.
   */
  private function getReturn($response, $data = array()) {
    $return = array();

    $return['headers'] = $response->getHeaders();
    $return['url'] = $response->getEffectiveUrl();
    $return['reason'] = $response->getReasonPhrase();
    $return['status_code'] = $response->getStatusCode();
    $return['protocol'] = $response->getProtocolVersion();
    $return['data'] = $data;

    return $return;
  }

}

/**
 * Class SalesCloudGrantTypeBase.
 */
abstract class SalesCloudGrantTypeBase implements GrantTypeInterface {
  /**
   * Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Config.
   *
   * @var \GuzzleHttp\Collection
   */
  protected $config;

  /**
   * Grant type.
   *
   * @var string
   */
  protected $grantType = '';

  /**
   * SalesCloudGrantTypeBase constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The Client.
   * @param array $config
   *   The config of the client.
   */
  public function __construct(ClientInterface $client, array $config = []) {
    $this->client = $client;
    $this->config = Collection::fromConfig($config, $this->getDefaults(), $this->getRequired());
  }

  /**
   * Get default configuration items.
   *
   * @return array
   *   Config defaults.
   */
  protected function getDefaults() {
    return [
      'client_secret' => '',
      'scope' => '',
      'token_url' => 'oauth2/token',
      'auth_location' => 'headers',
    ];
  }

  /**
   * Get required configuration items.
   *
   * @return string[]
   *   Client ID.
   */
  protected function getRequired() {
    return ['client_id'];
  }

  /**
   * Get additional options, if any.
   *
   * @return array|null
   *   Additional options.
   */
  protected function getAdditionalOptions() {
    return NULL;
  }

  /**
   * Gets token.
   *
   * @inheritdoc
   */
  public function getToken() {
    $config = $this->config->toArray();

    $body = $config;
    $body['grant_type'] = $this->grantType;
    unset($body['token_url'], $body['auth_location']);

    $request_options = [];

    if ($config['auth_location'] !== 'body') {
      $request_options['auth'] = [$config['client_id'], $config['client_secret']];
      unset($body['client_id'], $body['client_secret']);
    }

    $request_options['json'] = $body;

    if ($additional_options = $this->getAdditionalOptions()) {
      $request_options = array_merge_recursive($request_options, array($additional_options));
    }

    $response = $this->client->post($config['token_url'], $request_options);
    $data = $response->json();

    return new AccessToken($data['access_token'], $data['token_type'], $data);
  }

}

/**
 * Resource owner password credentials grant type.
 *
 * @link http://tools.ietf.org/html/rfc6749#section-4.3
 */
class SalesCloudPasswordCredentials extends SalesCloudGrantTypeBase {
  protected $grantType = 'password';

  /**
   * Gets required data.
   *
   * @inheritdoc
   */
  protected function getRequired() {
    return array_merge(parent::getRequired(), ['username', 'password']);
  }

}
